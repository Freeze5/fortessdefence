﻿using UnityEngine;
using Zenject;

public enum ArcherType
{
	Green,
	Red,
	Yellow
}

[RequireComponent(typeof(Animator), typeof(RectTransform))]
public class ArcherBase : MonoBehaviour
{
	[SerializeField] protected ArrowConfigs _arrowConfigs;
	[SerializeField] private Sprite _iconSprite;
	[SerializeField] private Pool _arrowsPool;
	private ChooseManager _chooseManager;
	private Vector2 _localPoint;
	
	public ArcherType ArcherType;
	public Sprite GetSprite => _iconSprite;
	protected Animator _animator;
	protected RectTransform _rectTransform;

	private void Awake()
	{
		_animator = GetComponent<Animator>();
		_rectTransform = GetComponent<RectTransform>();
		_chooseManager = FindObjectOfType<ChooseManager>();
	}

	private void OnEnable()
	{
		_chooseManager.OnReadyToShoot += StartShooting;
	}

	private void OnDisable()
	{
		_chooseManager.OnReadyToShoot -= StartShooting;
	}

	protected virtual void Shoot()
	{
		GameObject arrowGameObject = _arrowsPool.Get();
		if (arrowGameObject != null)
		{
			Arrow arrow = arrowGameObject.GetComponent<Arrow>();
			if (arrow != null)
			{
				arrow.gameObject.SetActive(true);
				arrow.Speed = _arrowConfigs.ArrowSpeed;
				arrow.Damage = _arrowConfigs.ArrowDamage;
				arrow.transform.localPosition = this.transform.localPosition;
				arrow.transform.localScale = Vector3.one;

				RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, Input.mousePosition, Camera.main, out _localPoint);

				Vector3 dir = _rectTransform.anchoredPosition - _localPoint;
				float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

				arrow.Fly(-dir.normalized, Quaternion.AngleAxis(angle, Vector3.forward));

			}
		}
	}

	private void StartShooting()
	{
		_animator.SetTrigger(ConstName.START);
	}

}
