﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class Arrow : MonoBehaviour , IPoolableObject
{
	private RectTransform _rectTransform;
	[SerializeField] private int _lifeTime;
	private int _initialLifeTime;
	private bool isFly;
	private Rigidbody2D _rb;
	public float Speed { get; set ;}
	public float Damage { get; set; }

	public Pool CurrentPool { get; set; }

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_rb = GetComponent<Rigidbody2D>();
		_initialLifeTime = _lifeTime;
	}

	private void OnEnable()
	{
		StartCoroutine(ArrowLifeTime());
	}

	public void Fly(Vector2 direction , Quaternion rotationAngle)
	{
		_rectTransform.rotation = rotationAngle;
		isFly = true;
		StartCoroutine(MoveToDirection(direction));
	}

	private IEnumerator MoveToDirection(Vector2 direction)
	{
		while (isFly)
		{
			_rectTransform.anchoredPosition += direction * Speed;
			yield return null;
		}
	}

	private IEnumerator ArrowLifeTime()
	{
		while(_lifeTime > 0)
		{
			yield return new WaitForSeconds(1);
			_lifeTime--;
		}
		_lifeTime = _initialLifeTime;
		isFly = false;
		_rectTransform.anchoredPosition = Vector2.zero;
		CurrentPool.Return(this.gameObject);
	}

}
