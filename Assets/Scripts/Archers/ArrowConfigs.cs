﻿using UnityEngine;
[CreateAssetMenu(menuName ="CreateArrowConfigs", fileName ="ArrowConfigs")]

public class ArrowConfigs : ScriptableObject
{
	public float ArrowSpeed;
	public float ArrowDamage;
}
