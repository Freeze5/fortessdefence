﻿using DG.Tweening;
using UnityEngine;
using Zenject;


public class EnemyBomber : BaseEnemy
{
	[Inject] private BomberPoolController _bomberPoolController;
	private float _currentHealth;
	private float _maxHealth;

	private void Start()
	{
		Initialize();
	}

	protected override void Initialize()
	{
		transform.localPosition = Vector3.zero;
		_currentHealth = _maxHealth = _enemyConfigs.Health;
		transform.localScale = Vector3.one;
		transform.localRotation = Quaternion.identity;
		_healthBarIcon.fillAmount = 1;
		Move();
	}

	protected override void Attack()
	{
		transform.DOKill();
		_animator.SetTrigger(ConstName.ATTACK);
	}

	protected override void Move()
	{
		transform.DOMoveX(CameraBorders.HorizontalBorder, _enemyConfigs.MoveDuration);
	}

	protected override void ReceiveDamage(float damage)
	{
		if (_currentHealth > 0)
		{
			_currentHealth -= damage;
			_healthBarIcon.fillAmount = _currentHealth / _maxHealth;
		}
		else
			Death();
			
	}

	protected override void Death()
	{
		_animator.SetTrigger(ConstName.DEATH);
	}

	public void EnemyDied()
	{
		transform.DOKill();
		_bomberPoolController.RemoveBomber();
	}

	public class BomberPool: MonoMemoryPool<EnemyBomber>
	{
		protected override void Reinitialize(EnemyBomber item)
		{
			item.Initialize();
		}
	}
}
