﻿using UnityEngine;
[CreateAssetMenu(menuName ="CreateEnemyConfig", fileName = "EnemyConfig")]
public class EnemyConfigs : ScriptableObject
{
	public float Damage;
	public float Health;
	public float MoveDuration;

}
