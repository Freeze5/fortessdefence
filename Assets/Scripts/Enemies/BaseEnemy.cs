﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public abstract class BaseEnemy : MonoBehaviour
{
	protected Animator _animator;
	[SerializeField] protected EnemyConfigs _enemyConfigs;
	[SerializeField] protected Image _healthBarIcon;
	public static event Action<float> OnCastleDamage;
	private Color _baseColor;

	private void Awake()
	{
		_animator = GetComponent<Animator>();
		_baseColor = Color.white;
	}

	protected abstract void Initialize();
	protected abstract void Attack();
	protected abstract void Move();
	protected abstract void ReceiveDamage (float damage);
	protected abstract void Death();

	//Called from AnimationEvent
	public void DamageCastle()
	{
		OnCastleDamage?.Invoke(_enemyConfigs.Damage);
	}

	protected virtual void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag(ConstName.CASTLE))
		{
			Attack();
		}
		if (collision.gameObject.GetComponent<Arrow>())
		{
			Arrow arrow = collision.gameObject.GetComponent<Arrow>();
			ReceiveDamage(arrow.Damage);
			StartCoroutine(ChangeColor(Color.red));
			arrow.CurrentPool.Return(arrow.gameObject);
		}
	}

	private IEnumerator ChangeColor(Color color)
	{
		this.gameObject.GetComponent<Image>().color = color;
		yield return new WaitForSeconds(0.2f);
		this.gameObject.GetComponent<Image>().color = _baseColor;
	}
}
