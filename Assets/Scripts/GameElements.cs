﻿using UnityEngine;

[CreateAssetMenu(menuName ="CreateGameElements", fileName ="GameElements")]
public class GameElements : ScriptableObject
{
	public GameObject BatPrefab;
	public GameObject OnagerPrefab;
	public GameObject BomberPrefab;
}
