﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
	private static GameManager _instance;
	public static GameManager Instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = (GameManager)FindObjectOfType(typeof(GameManager));
				return _instance;
			}
			return _instance;
		}
	}

	[Inject] private SceletonsCheckerManager _sceletonsChecker;
	[Inject] private BatPoolController _batPool;
	[Inject] private OnagerPoolController _onagerPool;
	[Inject] private BomberPoolController _bomberPool;
	[SerializeField]private int _gameStage;
	[SerializeField] private int _maxStages = 2;
	[SerializeField] private float _timer;
	[SerializeField] private float _maxTimer =10;

	private void OnEnable()
	{
		_sceletonsChecker.OnAllEnemiesDead += LevelCompleted;
	}

	private void OnDisable()
	{
		_sceletonsChecker.OnAllEnemiesDead -= LevelCompleted;
	}

	private void Start()
	{
		SpawnerController(5,7,8);
	}

	private void Update()
	{
		_timer += Time.deltaTime;
	}

	public void LoadStage()
	{
		if(_gameStage > _maxStages)
		{
			StateManager.Instance.ChangeState(StateType.WonGame);
			Debug.Log("You Won!");
			return;
		}
		switch (_gameStage)
		{
			case 1:
				_timer = 0;
				_maxTimer += 5;
				SpawnerController(4, 6, 7);
				break;
			case 2:
				_timer = 0;
				_maxTimer += 5;
				SpawnerController(5, 5, 5);
				break;
		}
	}

	private void SpawnerController( float batDelay , float bomberDelay , float OnagerDelay)
	{
		StartCoroutine(BatsSpawner(batDelay));
		StartCoroutine(OnagerSpawner(bomberDelay));
		StartCoroutine(BomberSpawner(OnagerDelay));
	}

	private IEnumerator BatsSpawner(float timeDelay)
	{
		while (_timer < _maxTimer)
		{
			yield return new WaitForSeconds(timeDelay);
			_batPool.AddBat();
		}
	}

	private IEnumerator OnagerSpawner(float timeDelay)
	{
		while (_timer < _maxTimer)
		{
			yield return new WaitForSeconds(timeDelay);
			_onagerPool.AddOnager();
		}
	}

	private IEnumerator BomberSpawner(float timeDelay)
	{
		while (_timer < _maxTimer)
		{
			yield return new WaitForSeconds(timeDelay);
			_bomberPool.AddBomber();
		}
	}

	private void LevelCompleted()
	{
		StateManager.Instance.ChangeState(StateType.WinLevel);
		_gameStage++;
	}
}
