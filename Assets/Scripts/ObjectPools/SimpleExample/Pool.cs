﻿using System.Collections.Generic;
using UnityEngine;


public class Pool : MonoBehaviour
{
		[SerializeField]
		private GameObject _prefab;

		private Queue<GameObject> _objectPool = new Queue<GameObject>();

		public void Add(int number)
		{
			for (int i = 0; i < number; i++)
			{
				GameObject entity = Instantiate(_prefab);
				entity.transform.SetParent(this.gameObject.transform);
				entity.GetComponent<IPoolableObject>().CurrentPool = this;
				entity.SetActive(false);
				_objectPool.Enqueue(entity);
			}
		
		}

		public GameObject Get()
		{
			if (_objectPool.Count == 0)
			{
				Add(1);
			}
			return _objectPool.Dequeue();
		}

		public void Return(GameObject objectToReturn)
		{
			objectToReturn.SetActive(false);
			_objectPool.Enqueue(objectToReturn);
		}
}

