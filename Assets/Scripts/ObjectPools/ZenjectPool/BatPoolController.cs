﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using System.Collections;
using System.Linq;

public class BatPoolController : MonoBehaviour
{
	[Inject] private BatEnemy.BatPool _batPool;
	public event Action<GameObject> OnBatSpawned;
	public event Action<GameObject> OnBatDied;
	[SerializeField] private List<BatEnemy> _bats = new List<BatEnemy>();


	public void AddBat()
	{
		_bats.Add(_batPool.Spawn());
		OnBatSpawned?.Invoke(_bats.LastOrDefault().gameObject);
	}

	public void RemoveBat()
	{
		var bat = _bats.FirstOrDefault();
		OnBatDied?.Invoke(bat.gameObject);
		_batPool.Despawn(bat);
		_bats.Remove(bat);
	}

}
