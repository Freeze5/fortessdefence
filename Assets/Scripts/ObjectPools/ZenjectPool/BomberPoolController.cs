﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using System;

public class BomberPoolController : MonoBehaviour
{
	[Inject] private EnemyBomber.BomberPool _bomberPool;
	public event Action<GameObject> OnBomberSpawned;
	public event Action<GameObject> OnBomberDied;
	[SerializeField] private List<EnemyBomber> _bombers = new List<EnemyBomber>();

	public void AddBomber()
	{
		_bombers.Add(_bomberPool.Spawn());
		OnBomberSpawned?.Invoke(_bombers.LastOrDefault().gameObject);
	}

	public void RemoveBomber()
	{
		var bomber = _bombers.FirstOrDefault();
		OnBomberDied?.Invoke(bomber.gameObject);
		_bomberPool.Despawn(bomber);
		_bombers.Remove(bomber);
	}
}
