﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using System;

public class OnagerPoolController : MonoBehaviour
{
	[Inject] private OnagerEnemy.OnagerPool _onagerPool;
	public event Action<GameObject> OnOnagerSpawned;
	public event Action<GameObject> OnOnagerDied;
	[SerializeField] private List<OnagerEnemy> _onagers = new List<OnagerEnemy>();

	public void AddOnager()
	{
		_onagers.Add(_onagerPool.Spawn());
		OnOnagerSpawned?.Invoke(_onagers.LastOrDefault().gameObject);
	}

	public void RemoveOnager()
	{
		var onager = _onagers.FirstOrDefault();
		OnOnagerDied?.Invoke(onager.gameObject);
		_onagerPool.Despawn(onager);
		_onagers.Remove(onager);
	}
}
