﻿public class ConstName
{
	public const string ATTACK = "Attack";
	public const string DEATH = "Death";
	public const string ENEMY = "Enemy";
	public const string CASTLE = "Castle";
	public const string START = "Start";
	public const string STOP = "Stop";
	public const string IN = "In";
	public const string OUT = "Out";
	public const string EMPTY = "Empty";
	public const string GAME = "BattleScene";
	public const string MainMenu = "MainMenu";
	public const string CURRENTLEVEL = "Level";
}