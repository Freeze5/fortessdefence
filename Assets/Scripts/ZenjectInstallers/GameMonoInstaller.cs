﻿using Zenject;
using UnityEngine;

public class GameMonoInstaller : MonoInstaller<GameMonoInstaller>
{
	[Inject] private GameElements _gameElem;

	public RectTransform BatSpawnPos;
	public RectTransform BomerSpawnPos;
	public RectTransform OnagerSpawnPos;

	public override void InstallBindings()
	{
		InstallSignals();
		InstallPools();
	}

	private void InstallPools()
	{
		Container.BindMemoryPool<BatEnemy, BatEnemy.BatPool>()
			.WithInitialSize(5)
			.FromComponentInNewPrefab(_gameElem.BatPrefab)
			.UnderTransform(BatSpawnPos);

		Container.BindMemoryPool<OnagerEnemy, OnagerEnemy.OnagerPool>()
			.WithInitialSize(5)
			.FromComponentInNewPrefab(_gameElem.OnagerPrefab)
			.UnderTransform(OnagerSpawnPos);

		Container.BindMemoryPool<EnemyBomber, EnemyBomber.BomberPool>()
			.WithInitialSize(5)
			.FromComponentInNewPrefab(_gameElem.BomberPrefab)
			.UnderTransform(BomerSpawnPos);
	}

	private void InstallSignals()
	{
		SignalBusInstaller.Install(Container);
	}
}
