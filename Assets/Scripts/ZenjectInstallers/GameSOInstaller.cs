﻿using Zenject;
using UnityEngine;

[CreateAssetMenu(menuName = "Create SOInstaller", fileName = "ScriptableObjectsInstaller")]
public class GameSOInstaller : ScriptableObjectInstaller<GameSOInstaller>
{
	[SerializeField] private GameElements _gameElements;

	public override void InstallBindings()
	{
		Container.BindInstance(_gameElements);
	}
}
