﻿using UnityEngine;

public static class CameraBorders
{
	private static float _xBorder = 0;
	private static float _yBorder = 0;

	public static float HorizontalBorder
	{
		get
		{
			if(_xBorder == 0)
			{
				Camera cam = Camera.main;
				_xBorder = ((float)Screen.width/(float)Screen.height) * cam.orthographicSize;
			}
			return _xBorder;
		}
	}

	public static float VerticalBorder
	{
		get
		{
			if (_yBorder == 0)
			{
				Camera cam = Camera.main;
				_yBorder = cam.orthographicSize;
			}
			return _yBorder;
		}
	}
}
