﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;
using DG.Tweening;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler , IDropHandler
{
	[Inject] private Canvas _canvas;
	private RectTransform _rectTransform;
	private CanvasGroup _canvasGroup;
	private Vector3 _startPos;
	public bool isCatched;
	public ArcherType ArcherType;

	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_canvasGroup = GetComponent<CanvasGroup>();
		_startPos = _rectTransform.localPosition;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isCatched = false;
		_canvasGroup.alpha = 0.6f;
		_canvasGroup.blocksRaycasts = false;
	}

	public void OnDrag(PointerEventData eventData)
	{
		_rectTransform.anchoredPosition += eventData.delta/_canvas.scaleFactor;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (!isCatched)
		{
			_rectTransform.DOLocalMove(_startPos, 0.5f);
		}
		_canvasGroup.alpha = 1f;
		_canvasGroup.blocksRaycasts = true;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		Debug.Log("Pointer down");
	}

	public void OnDrop(PointerEventData eventData)
	{
		throw new System.NotImplementedException();
	}
}
