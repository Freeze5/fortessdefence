﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class ChooseContainer : MonoBehaviour , IDropHandler
{
	private RectTransform _rectTransform;
	private Image _image;
	[Inject] private ChooseManager _chooseManager;


	private void Awake()
	{
		_rectTransform = GetComponent<RectTransform>();
		_image = GetComponent<Image>();
	}

	public void OnDrop(PointerEventData eventData)
	{
		if(eventData.pointerDrag != null)
		{
			var catchedRectTransform = eventData.pointerDrag.GetComponent<RectTransform>();
			catchedRectTransform.anchoredPosition = _rectTransform.anchoredPosition;

			var catchedScript = eventData.pointerDrag.GetComponent<DragDrop>();
			catchedScript.isCatched = true;
			catchedScript.gameObject.GetComponent<Image>().enabled = false;
			_image.enabled = false;
			InitializeArcher(catchedScript.ArcherType);
		}
		Debug.Log("Drop Cought");
	}


	private void InitializeArcher(ArcherType type)
	{
		var archer = Instantiate(_chooseManager.GetArcher(type));
		archer.gameObject.transform.SetParent(this.transform);
		archer.gameObject.transform.localScale = Vector3.one;
		archer.gameObject.transform.localPosition = transform.localPosition;
		_chooseManager.CheckAvailableArhcers();
	}
	
}
