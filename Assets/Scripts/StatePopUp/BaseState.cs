﻿using UnityEngine;
using DG.Tweening;

public abstract class BaseState
{
	private GameObject _prefab;
	protected virtual GameObject prefab
	{
		get
		{
			if (_prefab == null)
			{
				_prefab = StateManager.Instance.GetPopUp(type.ToString());
			}

			return _prefab;
		}
	}

	private IBehaviourPopUp _behaviour;
	protected IBehaviourPopUp behaviourPopUp
	{
		get
		{
			_behaviour = _behaviour ?? prefab.GetComponent<IBehaviourPopUp>();

			return _behaviour;
		}
	}

	public abstract StateType type { get; }
	public bool isActive { get; protected set; }

	public virtual void Load()
	{
		isActive = true;
		ShowFade();
	}

	public virtual void Unload(bool playCloseSound = true)
	{
		isActive = false;

		if (behaviourPopUp != null)
		{
			behaviourPopUp.Hide();
		}

		HideFade();
	}

	private void ShowPopUp()
	{
		behaviourPopUp.Show();
	}

	public void ShowFade()
	{
		StateManager.Instance.Fade.DOKill();
		StateManager.Instance.Fade.DOFade(0.6f, 0.3f).OnComplete(ShowPopUp);
		StateManager.Instance.Fade.raycastTarget = isActive;
	}

	private void HideFade()
	{
		StateManager.Instance.Fade.DOKill();
		StateManager.Instance.Fade.DOFade(0f, 0.3f).SetDelay(0.3f).OnComplete(DisableInteractive);
	}

	private void DisableInteractive()
	{
		StateManager.Instance.Fade.raycastTarget = isActive;
	}
}
