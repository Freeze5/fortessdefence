﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public abstract class BehaviourPopUp : MonoBehaviour, IBehaviourPopUp 
{
	[SerializeField]
    protected Animator _appearAnimation;

	private float _height = 2048;
	
	private int _isHaveOwnCamera = 0;
	protected int isHaveOwnCamera
	{
		get
		{
			if (_isHaveOwnCamera == 0)
			{
				_isHaveOwnCamera = this.gameObject.GetComponent<Camera>() != null ? 1 : -1;
			}

			return _isHaveOwnCamera;
		}
		private set
		{
			_isHaveOwnCamera = value;
		}
	}

	private Transform _transform;
    public Transform transformPopUp
    {
        get
        {
            if(_transform == null)
            {
                _transform = this.gameObject.GetComponent<Transform>();
            }

            return _transform;
        }
    }

    private RectTransform _rectTranform;
    public RectTransform rectTransformPopUp
    {
        get
        {
			if(_transform == null)
				_rectTranform = this.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<RectTransform>();

            return _rectTranform;
        }
    }

	public virtual void Show()
	{
        StopAllCoroutines();
		InputListener.OnReturn += OnReturnClick;
		
		if(_appearAnimation != null)
		{
            _appearAnimation.SetTrigger(ConstName.IN);
		}
		else
		{
			if (isHaveOwnCamera == 1)
			{
				rectTransformPopUp.anchoredPosition3D = new Vector3(0, _height, 0);
				rectTransformPopUp.DOAnchorPos3D(Vector3.zero, 0.8f).SetEase(Ease.OutBack);
			}
			else
			{
				transformPopUp.position = new Vector3(0, _height, 0);
				transformPopUp.DOMove(new Vector3(0,0,0), 0.8f).SetEase(Ease.OutBack);
			}
		}
	}

	protected virtual void OnReturnClick()
	{
		CloseButton();
	}

	protected virtual void EnabledPopUp()
	{
		this.gameObject.SetActive(true);
	}

	public virtual void Hide()
	{
		InputListener.OnReturn -= OnReturnClick;
		
		if(_appearAnimation != null)
		{
            if(gameObject.activeSelf)
                StartCoroutine(WaitEndDisable());
		}
		else
		{
			if(isHaveOwnCamera == 1)
				rectTransformPopUp.DOAnchorPos3D(new Vector3(0, -_height, 0), 0.6f).OnComplete(DisabledPopUp);
	        else
				transformPopUp.DOMove(new Vector3(0, -_height, 0), 0.6f).OnComplete(DisabledPopUp);
		}
	}

	protected virtual void DisabledPopUp()
	{
		this.gameObject.SetActive(false);
	}

	public virtual void CloseButton()
	{
		StateManager.Instance.ChangeState(StateType.None);
	}

    protected IEnumerator WaitEndDisable()
    {
        yield return StartCoroutine(AnimationControl.PlayAnimationCoroutine(_appearAnimation, ConstName.OUT, ConstName.EMPTY));
        DisabledPopUp();
    }
}