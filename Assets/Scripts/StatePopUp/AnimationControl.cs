﻿using UnityEngine;
using System.Collections;

public class AnimationControl
{
	public static IEnumerator PlayAnimationCoroutine(Animator animator, string trigerName, string waitForState)
	{
		if (animator != null)
		{
			animator.SetTrigger(trigerName);
			do
				yield return null;
			while (!animator.GetCurrentAnimatorStateInfo(0).IsName(waitForState));
		}
		yield return null;
	}
}