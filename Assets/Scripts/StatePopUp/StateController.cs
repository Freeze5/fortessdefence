﻿public enum StateType
{
	None,
	WinLevel,
	LoseLevel,
	WonGame
}

public class StateController
{
	private BaseState _currentState;
	private bool _isSetValue = false;
	private WinState _winState = new WinState();
	private LoseState _loseState = new LoseState();
	private WonGameState _wonGameState = new WonGameState();

	public void ChangeState(StateType newState)
	{
		if (_isSetValue)
			_currentState.Unload();

		_isSetValue = true;

		SetState(newState);

		if (_isSetValue)
			_currentState.Load();
	}

	public StateType GetCurrentState()
	{
		return _currentState != null ? _currentState.type : StateType.None;
	}

	public void ShowFade()
	{
		if (_currentState != null)
		{
			_currentState.ShowFade();
		}
	}

	public void SetState(StateType state)
	{
		switch (state)
		{
			case StateType.WinLevel:
				_currentState = _winState;
				break;
			case StateType.LoseLevel:
				_currentState = _loseState;
				break;
			case StateType.WonGame:
				_currentState = _wonGameState;
				break;
			case StateType.None:
				_currentState = null;
				_isSetValue = false;
				break;
		}
	}
}