﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputListener : MonoBehaviour, IPointerClickHandler
{
	public static event Action OnReturn;

	public void OnPointerClick(PointerEventData eventData)
	{
		if (OnReturn != null)
			OnReturn();
	}
}