﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{
	#region singlton

	private static StateManager instance = null;

	public static StateManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = (StateManager) FindObjectOfType(typeof(StateManager));
				if (instance == null)
				{
					GameObject stateManagerObject = Instantiate(Resources.Load("Prefabs/StateManagerPrefab")) as GameObject;
					instance = stateManagerObject.GetComponent<StateManager>();
					instance.Fade = stateManagerObject.GetComponentInChildren<Image>();
					instance._canvas = stateManagerObject.GetComponent<Canvas>();
					instance._canvas.worldCamera = Camera.main;
				}

				DontDestroyOnLoad(instance);
			}

			return instance;
		}
	}

	private StateManager(){}

	#endregion

	private Dictionary<string, GameObject> _popUps = new Dictionary<string, GameObject>();

	private Canvas _canvas;
	
	private StateController _stateController;
	public StateController stateController
	{
		get
		{
			_stateController = _stateController ?? new StateController();
			return _stateController;
		}
	}

	public bool isOpeningState
	{
		get { return stateController.GetCurrentState() != StateType.None; }
	}

	private Image _fade;
	public Image Fade
	{
		get
		{
			if (Instance._canvas.worldCamera == null)
				Instance._canvas.worldCamera = Camera.main;
			return _fade;
		}

		private set { _fade = value; }
	}

	public void ChangeState(StateType state)
	{
		stateController.ChangeState(state);
	}

	public StateType currentState
	{
		get { return stateController.GetCurrentState(); }
		set { stateController.SetState(value); }
	}

	public GameObject GetPopUp(string idPopUp)
	{
		GameObject popup = null;

		_popUps.TryGetValue(idPopUp, out popup);

		if (popup == null)
		{
			popup = Resources.Load<GameObject>("Prefabs/PopUp/" + idPopUp);
			popup = MonoBehaviour.Instantiate(popup);
			popup.name = idPopUp;

			DontDestroyOnLoad(popup);

			_popUps.Add(idPopUp, popup);
		}

		return popup;
	}
}