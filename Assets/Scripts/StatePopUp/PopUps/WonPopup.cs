﻿using UnityEngine.SceneManagement;

public class WonPopup : BehaviourPopUp
{
	public override void Show()
	{
		base.EnabledPopUp();
		base.Show();
	}

	public override void Hide()
	{
		base.Hide();
	}

	public void CloseButtonClick()
	{
		SceneManager.LoadScene(ConstName.MainMenu);
		base.CloseButton();
		base.Hide();
	}
}
