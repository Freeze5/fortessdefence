﻿using UnityEngine.SceneManagement;

public class LosePopup : BehaviourPopUp
{
	public override void Show()
	{
		base.EnabledPopUp();
		base.Show();
	}
    
	public override void Hide()
	{
		base.Hide();
	}
	
	public void CloseButtonClick()
	{
		base.CloseButton();
		base.Hide();
		SceneManager.LoadScene(ConstName.GAME);
	}
}
