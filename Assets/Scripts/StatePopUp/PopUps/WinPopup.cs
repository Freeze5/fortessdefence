﻿
public class WinPopup : BehaviourPopUp
{
	public override void Show()
	{
		base.EnabledPopUp();
		base.Show();
	}
    
	public override void Hide()
	{
		base.Hide();
	}
	
	public void CloseButtonClick()
	{
		GameManager.Instance.LoadStage();
		base.CloseButton();
		base.Hide();
	}
}
