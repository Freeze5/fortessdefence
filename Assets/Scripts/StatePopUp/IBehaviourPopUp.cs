﻿public interface IBehaviourPopUp
{
	void Show();
	void Hide();
}