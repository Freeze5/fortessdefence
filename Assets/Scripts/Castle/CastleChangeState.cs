﻿using UnityEngine;

[CreateAssetMenu(menuName ="CreateCastleState" , fileName ="CastleState")]
public class CastleChangeState : ScriptableObject
{
	public Sprite Sprite;
	public float HealthPersent;
}
