﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Castle : MonoBehaviour 
{
	[SerializeField] private List<CastleChangeState> _castleStates;
	[SerializeField] private Image _image;


	private float _currentHealth;
	private float _maxHealth;

	public void SetHealth(float maxHealth)
	{
		_currentHealth = _maxHealth = maxHealth;
	}

	public void ChangeHealth(float damage)
	{
		if (_currentHealth > 0)
		{
			_currentHealth -= damage;
			CheckState();
		}
	}

	private void CheckState()
	{
		foreach (CastleChangeState state in _castleStates)
		{
			if ((_currentHealth / _maxHealth) * 100f >= state.HealthPersent)
			{
				_image.sprite = state.Sprite;
				break;
			}
		}
	}
}
