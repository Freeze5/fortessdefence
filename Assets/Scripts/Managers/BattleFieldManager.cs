﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


public class BattleFieldManager : MonoBehaviour
{
	[Inject] private Castle _castle;

	[SerializeField] private Image _castleHealthBar;

	[SerializeField] private float _maxHealthCastle;
	[SerializeField] private float _currHealthCastle;

	void Start()
	{
		InitializeCastleStats();
	}


	private void OnEnable()
	{
		BaseEnemy.OnCastleDamage += CheckCastleHP;
	}
	private void OnDisable()
	{
		BaseEnemy.OnCastleDamage -= CheckCastleHP;
	}

	private void InitializeCastleStats()
	{
		_currHealthCastle = _maxHealthCastle;
		_castleHealthBar.fillAmount = _currHealthCastle / _maxHealthCastle;
		_castle.SetHealth(_maxHealthCastle);
	}

	private void CheckCastleHP(float receivedDamage)
	{
		_currHealthCastle -= receivedDamage;
		if (_currHealthCastle > 0)
		{
			_castleHealthBar.fillAmount = _currHealthCastle / _maxHealthCastle;
			_castle.ChangeHealth(receivedDamage);
		}
		else
		{
			BaseEnemy.OnCastleDamage -= CheckCastleHP;
			StateManager.Instance.ChangeState(StateType.LoseLevel);
			Debug.Log("Castle has been destroyed!!!");
		}
	}
}
