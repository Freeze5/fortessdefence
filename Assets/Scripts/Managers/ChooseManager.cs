﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class ChooseManager : MonoBehaviour
{
	[SerializeField] private List<ArcherBase> _archers = new List<ArcherBase>();
	private Dictionary<ArcherType, ArcherBase> _archersDict = new Dictionary<ArcherType, ArcherBase>();
	private bool isInitialized;
	private int _itemsCount;
	public event Action OnReadyToShoot;

	private void Start()
	{
		InitializeDictionaty();
		_itemsCount = _archers.Count;
	}

	private void InitializeDictionaty()
	{
		_archersDict.Clear();
		foreach (ArcherBase archer in _archers)
		{
			_archersDict.Add(archer.ArcherType, archer);
			Debug.Log("Added archer with type: " + archer.ArcherType);
		}
		isInitialized = true;
	}

	public ArcherBase GetArcher (ArcherType type)
	{
		if (!isInitialized)
			InitializeDictionaty();

		var archer	= _archersDict[type];
		_itemsCount--;
		return archer;
	}

	public void CheckAvailableArhcers()
	{
		if(_itemsCount <= 0)
		{
			OnReadyToShoot?.Invoke();
		}
	}


}
