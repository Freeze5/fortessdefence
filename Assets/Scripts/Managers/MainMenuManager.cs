﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour 
{

	public void PlayButtonClick()
	{
		SceneManager.LoadScene(ConstName.GAME);
	}
	
	public void QuitButtonClick()
	{
		Application.Quit();
	}
}
