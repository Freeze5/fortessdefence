﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
public class SceletonsCheckerManager : MonoBehaviour
{
	[Inject] private BatPoolController _batPool;
	[Inject] private OnagerPoolController _onagerPool;
	[Inject] private BomberPoolController _bomberPool;
	[SerializeField] private List<GameObject> _sceletons = new List<GameObject>();
	public event Action OnAllEnemiesDead;

	private void OnEnable()
	{
		_batPool.OnBatSpawned += AddToList;
		_batPool.OnBatDied += RemoveFromList;
		_bomberPool.OnBomberSpawned += AddToList;
		_bomberPool.OnBomberDied += RemoveFromList;
		_onagerPool.OnOnagerSpawned += AddToList;
		_onagerPool.OnOnagerDied += RemoveFromList;

	}

	private void OnDisable()
	{
		_batPool.OnBatSpawned -= AddToList;
		_batPool.OnBatDied -= RemoveFromList;
		_bomberPool.OnBomberSpawned -= AddToList;
		_bomberPool.OnBomberDied -= RemoveFromList;
		_onagerPool.OnOnagerSpawned -= AddToList;
		_onagerPool.OnOnagerDied -= RemoveFromList;
	}

	private void AddToList(GameObject entity)
	{
		_sceletons.Add(entity);
	}

	private void RemoveFromList(GameObject objToRemove)
	{
		_sceletons.Remove(objToRemove);
		if (IsAllEnemiesDead())
		{
			OnAllEnemiesDead?.Invoke();
		}
	}

	private bool IsAllEnemiesDead()
	{
		if (_sceletons.Count <= 0)
			return true;

		return false;
	}
}
